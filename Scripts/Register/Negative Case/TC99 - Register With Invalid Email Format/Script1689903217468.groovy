import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Positive Case/TC66 - Navigate To Register Page Using Text Link'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Register/editNama'), 0)

Mobile.sendKeys(findTestObject('Register/editNama'), 'Hasby Ramadhan')

Mobile.tap(findTestObject('Register/datePicker'), 0)

Mobile.tap(findTestObject('Register/yearPicker'), 0)

Mobile.scrollToText('2005')

Mobile.tap(findTestObject('Register/pick2005'), 0)

Mobile.tap(findTestObject('Register/buttonOKDate'), 0)

Mobile.tap(findTestObject('Register/editEmail'), 0)

Mobile.sendKeys(findTestObject('Register/editEmail'), 'hasbyramadhan01@gmail.com')

Mobile.tap(findTestObject('Register/editWhatsapp'), 0)

Mobile.sendKeys(findTestObject('Register/editWhatsapp'), '081265576016')

Mobile.tap(findTestObject('Register/editPassword'), 0)

Mobile.sendKeys(findTestObject('Register/editPassword'), 'hasby123')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Register/editConfirmPassword'), 0)

Mobile.sendKeys(findTestObject('Register/editConfirmPassword'), 'hasby123')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Register/checkBoxRegister'), 0)

Mobile.tap(findTestObject('Register/buttonDaftar'), 0)

Mobile.verifyElementVisible(findTestObject('Register/verifikasiEmailRegister'), 0)

